﻿using System.Collections.Generic;

namespace Interview_Coding_Assignment.Interfaces
{


    public interface IValuesReader
    {
        IEnumerable<string> GetValues();
    }

}
