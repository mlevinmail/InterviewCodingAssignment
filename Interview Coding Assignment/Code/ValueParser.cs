﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interview_Coding_Assignment.DataTypes;

namespace Interview_Coding_Assignment.Code
{
    public class ValueParser
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value">Value as read from the source</param>
        /// <param name="parsedResult">Parsed value </param>
        /// <returns>Parsing operation result</returns>
        public ValueParsedResult TryParseValue(string value, out double parsedResult)
        {


            // 1. Trying parsing the double as it is
            if (Double.TryParse(value, out double result))
            {
                parsedResult = result;
                return ValueParsedResult.Success;
            }

            var splitValue = value.Split(',');

            if (splitValue.Length > 0)
            {
                value = splitValue[0];
            }

            // 2. Eliminating all the chars that could be introduced 
            // inadvertently by user when typing the number.
            // ex. 123q.22 becomes 123.22
            var strippedValue = StripToDoubleFormat(value);



            if (Double.TryParse(strippedValue, out result))
            {

                parsedResult = result;
                return ValueParsedResult.PartialParse;

            }


            parsedResult = result;
            return ValueParsedResult.Fail;
        }



        private string StripToDoubleFormat(string value)
        {
            var result = new StringBuilder();
            for (var i = 0; i < value.Length; i++)
            {

                if (Char.IsDigit(value[i]) || value[i] == '.' || value[i] == ',')
                    result.Append(value[i]);
            }

            return result.ToString();
        }


    }
}
