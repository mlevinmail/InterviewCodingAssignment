﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using Interview_Coding_Assignment.Interfaces;

namespace Interview_Coding_Assignment.Code
{
    public class FileValuesReader : IValuesReader
    {

        private readonly string _fileName;
        private readonly List<string> _values = new List<string>();


        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName">Full path of the file </param>
        public FileValuesReader(string fileName)
        {
            if (!File.Exists(fileName))
            {
                throw new IOException("File does not exist");
            }

            _fileName = fileName;
        }


        /// <summary>
        /// Reads values from the file specified in a constructor
        /// </summary>
        /// <returns>Collection of values read from file line by line</returns>
        public IEnumerable<string> GetValues()
        {
            var line = "";
            var file = new StreamReader(_fileName);
            while ((line = file.ReadLine()) != null)
            {
                if (line != "")
                    _values.Add(line);
            }

            return _values;
        }
    }
}
