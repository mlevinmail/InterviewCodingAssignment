﻿using System.Collections.Generic;
using Interview_Coding_Assignment.Annotations;
using Interview_Coding_Assignment.Interfaces;

namespace Interview_Coding_Assignment.Code
{
    public class StoredValues
    {
        private readonly IValuesReader _valuesReader;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="valuesReader">An implementation of an interface that can retrieve values from</param>
        public StoredValues([NotNull] IValuesReader valuesReader)
        {
             _valuesReader = valuesReader;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns>Collection of values</returns>
        public IEnumerable<string> GetValues()
        {
            return _valuesReader.GetValues();
        } 


    }
}
