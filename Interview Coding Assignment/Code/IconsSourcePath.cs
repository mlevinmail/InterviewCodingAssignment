﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interview_Coding_Assignment.Code
{
    /// <summary>
    /// The class contains paths to the icons in the project. This is done in order to avoid hard coding paths in the code
    /// </summary>
    public static class IconsSourcePath
    {
        public static string FailIcon { get; } = @"Resources/Icons/icons8-error-50.png";
        public static string WarningIcon { get; } = @"Resources/Icons/icons8-warning-50.png";
        public static string SuccessIcon  {get; } = @"Resources/Icons/icons8-ok-50.png";

    }
}
