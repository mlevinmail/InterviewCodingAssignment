﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interview_Coding_Assignment.DataTypes
{
    /// <summary>
    /// Value parsed result 
    /// 
    /// Success - original value and parsed value are the same 
    /// 
    /// PartialParse - orignal value had to be modified for parsing
    /// 
    /// Fail - Nothing could be parsed from an orignial value 
    /// </summary>
    public enum ValueParsedResult
    {
        Success,
        PartialParse,        
        Fail      
    }
}
