﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Interview_Coding_Assignment.Annotations;
using Interview_Coding_Assignment.DataTypes;

namespace Interview_Coding_Assignment.Models
{

    /// <summary>
    /// Represent the model for the  ListView inside the main Window
    /// </summary>
    public class ParsedResultModel : INotifyPropertyChanged
    {
        public int Id { get; set; }


        private string _originalValue;
        public string OriginalValue
        {
            get { return _originalValue; }
            set
            {
                if (value == _originalValue) return;
                _originalValue = value;
                OnPropertyChanged("OriginalValue");
            }
        }


        private string _parsedValue;
        public string ParsedValue
        {
            get { return _parsedValue; }
            set
            {
                if (value == _parsedValue) return;
                _parsedValue = value;
                OnPropertyChanged("ParsedValue");
            }
        }

        private ValueParsedResult _valueParsedResult;
        public ValueParsedResult ValueParsedResult
        {
            get { return _valueParsedResult; }
            set
            {
                if (value == _valueParsedResult) return;
                _valueParsedResult = value;
                OnPropertyChanged("ValueParsedResult");
            }
        }

        private Brush _resultBackgroundColor;
        public Brush ResultBackgroundColor
        {
            get { return _resultBackgroundColor; }
            set
            {
                if (Equals(value, _resultBackgroundColor)) return;
                _resultBackgroundColor = value;
                OnPropertyChanged("ResultBackgroundColor");
            }
        }


        private string _imageSourcePath;
        public string ImageSourcePath
        {
            get { return _imageSourcePath; }
            set
            {
                if (value == _imageSourcePath) return;
                _imageSourcePath = value;
                OnPropertyChanged("ImageSourcePath");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
