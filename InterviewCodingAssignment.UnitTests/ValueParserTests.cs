﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interview_Coding_Assignment.Code;
using Interview_Coding_Assignment.DataTypes;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace InterviewCodingAssignment.UnitTests
{

    /// <summary>
    /// The values files that are generated are known to contain partial, error, or success values.
    /// Unit tests in this class verify ParseValue class by comparing the results to the parsing from these files
    /// </summary>
    [TestFixture]
    internal class ValueParserTests
    {


        // specify full path to the files
        private readonly string NoErrorCollectionPath = @"c:\testfiles\NoErrorCollection.txt";
        private readonly string ErrorCollectionPath = @"c:\testfiles\ErrorCollection.txt";
        private readonly string PartialCollectionPath = @"c:\testfiles\PartialCollection.txt";


        private readonly ValueParser _valueParser;

        public ValueParserTests()
        {
            _valueParser = new ValueParser();            
        }


        private IEnumerable<string> LoadCollectionFromFile(string filePath)
        {         
            var fileValuesReader = new FileValuesReader(filePath);
            var storedValues = new StoredValues(fileValuesReader);
            return storedValues.GetValues();
        }

        [Test]
        public void IsNoErrorParseCorrectTest()
        {
            var collection = LoadCollectionFromFile(NoErrorCollectionPath);
            Assert.Multiple(
                () =>
                {
                    foreach (var item in collection)
                    {
                        var result = _valueParser.TryParseValue(item, out double parsedResult);

                        // Checking that all the parsing was succefull 
                        Assert.AreEqual(ValueParsedResult.Success, result);

                        // Checking that orignial value is equal to the parsed one
                        Assert.AreEqual(parsedResult.ToString(CultureInfo.InvariantCulture).Trim(), item.Trim());
                    }
                });
        }

        [Test]
        public void IsPartialParseCorrectTest()
        {
            var collection = LoadCollectionFromFile(PartialCollectionPath);
            Assert.Multiple(
                () =>
                {
                    foreach (var item in collection)
                    {
                        var result = _valueParser.TryParseValue(item, out double parsedResult);
                        // Checking that all the parsing was succefull 
                        Assert.AreEqual(ValueParsedResult.PartialParse, result);
                    }
                });
        }

        [Test]
        public void IsErrorParseCorrectTest()
        {
            var collection = LoadCollectionFromFile(ErrorCollectionPath);
            Assert.Multiple(
                () =>
                {
                    foreach (var item in collection)
                    {
                        var result = _valueParser.TryParseValue(item, out double parsedResult);
                        // Checking that all the parsing was succefull 
                        Assert.AreEqual(ValueParsedResult.Fail, result);                    
                    }
                });
        }

    }
}
