﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interview_Coding_Assignment.Code;
using NUnit.Framework;

namespace InterviewCodingAssignment.UnitTests
{

    [TestFixture]
    internal class FileValuesReaderTests
    {

        [Test]
        public void ConstructorThrowsAnExceptionIfFileDoesNotExist()
        {
            Assert.Throws<System.IO.IOException>(() =>
            {
                var fileValuesReader = new FileValuesReader("some file path that does not exist");
            });

        }


      


    }

    
}
